---
layout: page
title: 使用R做全基因组关联分析（翻译）
tags: [bioinformatics]

---

Translated from "https://www.r-bloggers.com/genome-wide-association-studies-in-r"

# 历史背景
自十七世纪达尔文提出自然和性别选择学说后，基因(gene)的角色并没有被阐明，而我们已知基因可通过等位基因(allele)表示。
达尔文的后继者，孟德尔通过提出基因分离定律和自由组合定律，开始揭开遗传学的神秘面纱。
在此之后，遗传学先驱费舍尔提出的诸如遗产变异等概念，逐步揭示了表型（即可观测性状）的遗传基础
而后续出现的基因型概念又推动了多态遗传标记的发展。
早期，确定基因型主要通过分析等位位点（loci，染色体上区域的泛定义）的组成，而后出现了拷贝数变异(CNVs)，短连续重复序列（STRs）以及单核苷酸多态性(SNPs)
人类和包括植物在内的许多其它物种都是二倍体生物（即每条染色体都有连个拷贝），这意味这可以用等位基因A和a来表示两个等位基因，从而可通过AA，Aa和aa来区分个体。
一些疾病遗传基础的发现主要得益于其相对简单的遗传机制--变异少但外显率高，因而容易被发现。
今儿可以理解的是，已经长期存在且更复杂的多遗传因素疾病，如神经退行性老年痴呆症和帕金森仍然是研究的热点。
研究这些由多个微效基因相互作用产生的性状，需要获取大量的遗传资源并借助灵活的统计方法。值得庆幸的是，当代测序技术和高性能计算的发展使之成为现实。

# 全基因组关联分析（genome-wide associationstudies）
简而言之，GWAS通过寻找基因组上的所有变异位点，来查找与目标性状相关的SNPs。
值得注意的是，目标性状实际上可以是来自与群体中的任意表型，即数量性状（如身高）和质量性状（如是否患病）。
实际上，假定存在p个SNP和n个样本或个体，GWAS的原理是将建立ｐ个独立线性模型，每个模型都建立在n个样本上，而每个SNP的基因型作为目标性状的自变量。
一般而言，每个SNP的与目标性状关联是否显著，是由所建线性模型系数估计决定的。
而由于这些测验相互独立且数量巨大，高并发计算是必要的。
当然，校正多个假设检验的p值也是必要的，常用的方法有 Bonferroni, Benjamin-Hochberg, 或者FDR。
而现在GWAS在许多物种的研究中是很常见的分析。

# 关联分析和连锁分析
关联分析(association analysis)，连锁分析(linkage maping)和QTL定位(quantitative trait loci mapping)是容易混淆的三个概念。虽然概念上存在相似之处，但它们在实际使用中却差别很大。

One of the key differences between the two is that association mapping relies on high-density SNP genotyping of unrelated individuals, whereas linkage mapping relies on the segregation of substantially fewer markers in controlled breeding experiments – unsurprisingly QTL mapping is seldom conducted in humans.
Importantly, association mapping gives you point associations in the genome, whereas linkage mapping gives you QTL, chromosomal regions.

The present tutorial covers fundamental aspects to consider when conducting GWA analysis, from the pre-processing of genotype and phenotype data to the interpretation of results.
We will use a mixed population of 316 Chinese, Indian and Malay that was recently characterized using high-throughput SNP-chip sequencing, transcriptomics and lipidomics (Saw et al., 2017).
More specifically, we will search for associations between the >2.5 million SNP markers and cholesterol levels.
Finally, we will explore the vicinity of candidate SNPs using the USCS Genome Browser in order to gain functional insights.
The methodology shown here is largely based on the tutorial outlined in Reed et al., 2015.
The R scripts and some of the data can be found in my repository, but you will still need to download the omics data from here.
Please follow the instructions in the repo.

# 
