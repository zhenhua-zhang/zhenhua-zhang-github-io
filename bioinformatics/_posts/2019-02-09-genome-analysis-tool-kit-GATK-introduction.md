---
layout: page
title: Genome Analysis ToolKit -- Introduction
tags: [bioinformatics]
catogory: [tools]

---

[Genome Analysis ToolKit](https://software.broadinstitute.org/gatk) (GATK) is a
famous bioinformatic tool for variants discovery and genotyping.


### Best Practice
