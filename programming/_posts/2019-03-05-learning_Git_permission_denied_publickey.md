---
layout: page
title: Learning Git(1) -- Permission denied (publickey)
tags: [git]

---

How to solve `Permission Denied (publickey)` error when using `git push`  
Causation: Git failed use public key generated with a customized file name.  
Solution: Add `Host` information to `~/.ssh/config`  

```{bash}
Host github.com
	Hostname github.com
	IdentityFile ~/.ssh/id_rsa.github
	IdentitiesOnly yes
```

The `IdentiesOnly` is required to prevent the `ssh` default behaviour of sending
the identity file matching the default filename for each protocol. If you have 
a file named `~/.ssh/id_rsa` that will get tried before your 
`~/.ssh/id_rsa.github` without this option

