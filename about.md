---
layout: page
title: About
permalink: /about/
---

Zhenhua Zhang(振华 张).  
Bioinformatician,  
Born and raised in Jinan, China.  
Finished primary, middle and high school in his hometown, a lovely city, Jinan, China.  
Acquired bachelor and master degree in [China Agricultural University](http://www.cau.edu.cn), Beijing, China.  
Worked as a voluntary teacher in Pingwang Middle School, Fangchenggang, Guangxi.  
Working as a PhD candidate in [University Medical Centre of Groningen](https://www.umcg.nl), Groningen, the Netherlands.  
